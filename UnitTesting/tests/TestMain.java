import org.junit.Test;
import ro.orangetraining.junittests.*;

import static org.junit.Assert.assertEquals;

public class TestMain {
    @Test
    public void testSum(){
        int a = 5;
        int b = 5;
        int expected = 10;
        assertEquals( Main.sum(a,b) , expected );
    }
    @Test
    public void testMultiply(){
        int a = 5;
        int b = 5;
        int expected = 25;
        assertEquals(Main.multiply(a,b) , expected);
        a = 6;
        b = 3;
        expected = 18;
        assertEquals(Main.multiply(a,b), expected);
        a = 2;
        b = 255;
        expected = 510;
        assertEquals(Main.multiply(a,b), expected);
    }
    @Test
    public void testSquared(){
        {
            int x = 5;
            int y = 20;
            int expected = 425;
            assertEquals( Main.squared(x,y)   ,     expected);
        }
        int x = 4;
        int y = 10;
        int expected = -84;
        assertEquals( Main.squared(x,y),  expected);
    }
    @Test
    public void testSumMultiply(){
        int x = 5;
        int y = 2;
        int multiply1 = Main.multiply(x,y);

        int z = 2;
        int t = 5;
        int multiply2 = Main.multiply(z,t);
        int expected = 20;
        assertEquals(Main.sum(multiply1 , multiply2), expected);
    }
    @Test
    public void testXor2(){
        boolean a = true;
        boolean b = true;
        boolean expected = false;
        assertEquals(Main.xor(a,b), expected);

        a = true;
        b = false;
        expected = true;
        assertEquals(Main.xor(a,b), expected);

        a = false;
        b = true;
        expected = true;
        assertEquals(Main.xor(a,b), expected);

        a = false;
        b = false;
        expected = false;
        assertEquals(Main.xor(a,b), expected);


    }
    @Test
    public void testXor3(){
        boolean a, b, c, expected;

        a = true;
        b = true;
        c = true;
        expected = true;
        assertEquals(Main.xor(a,b,c), expected);

        a = true;
        b = true;
        c = false;
        expected = false;
        assertEquals(Main.xor(a,b,c), expected);

        a = true;
        b = false;
        c = false;
        expected = true;
        assertEquals(Main.xor(a,b,c), expected);

        a = false;
        b = false;
        c = false;
        expected = false;
        assertEquals(Main.xor(a,b,c), expected);

        a = true;
        b = false;
        c = true;
        expected = false;
        assertEquals(Main.xor(a,b,c), expected);

        a = false;
        b = false;
        c = true;
        expected = true;
        assertEquals(Main.xor(a,b,c), expected);

        a = false;
        b = true;
        c = true;
        expected = false;
        assertEquals(Main.xor(a,b,c), expected);

        a = false;
        b = true;
        c = false;
        expected = true;
        assertEquals(Main.xor(a,b,c), expected);

    }
    @Test
    public void testXor4() {
        boolean a, b, c, d, expected;
        a = true;
        b = true;
        c = true;
        d = true;
        expected = false;
        assertEquals(Main.xor(a, b, c, d), expected);
    }
    @Test
    public void testXorVargs(){
        boolean expected = true;
        assertEquals(Main.xorgeneral(true, true, true, true, true), expected);
        assertEquals(Main.xorgeneral(true, true, true, true), false);
        assertEquals(Main.xorgeneral(true, true, true, true, true, true), false);
        assertEquals(Main.xorgeneral(true, true, true), true);
        assertEquals(Main.xorgeneral(true, true), false);
        assertEquals(Main.xorgeneral(true), true);
        assertEquals(Main.xorgeneral(), false);
        assertEquals(Main.xorgeneral(true, true, true, false), true);
        assertEquals(Main.xorgeneral(true, true, false, true, true, true), true);

    }
}
