package ro.orangetraining.junittests;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int a = 5;
        int b = 2;
        int result = sum( multiply(a,b) , multiply(b,a) );

        System.out.println(result);
    }
    public static int sum(int a, int b){
        return a + b;
    }
    public static int multiply(int a, int b){
        return a * b;
    }
    public static int squared(int a, int b){
        if (a%2 == 1){
            return a*a + b*b;
        }
        else {
            return a * a - b * b;
        }
    }
    public static boolean xorsimple(boolean a, boolean b){
        if ( (a && !b ) || (!a && b)) {
            return true;
        }
        else {
            return false;
        }
    }
    public static boolean xor(boolean a, boolean b){
        if ( (a && !b ) || (!a && b)) {
            return true;
        }
        else {
            return false;
        }
    }
    // xor(a,b,c) ==?  a && xor(b,c)

    public static boolean xor(boolean a, boolean b, boolean c){

        return ( xor(a,  xor(b,c) ));
    }
    public static boolean xor(boolean a, boolean b, boolean c, boolean d){
        return ( xor(a,  xor(b, xor (c,d)) ));
    }

    public static boolean xorgeneral(boolean... args){
        if (args.length < 1) return false;
        if (args.length == 1) return args[0];
        if (args.length == 2) {
            if ( (args[0] && !args[1] ) || (!args[0] && args[1])) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            int n = args.length;
            boolean nextResults = xorgeneral(args[n-2], args[n-1] );
            boolean finalResults = xorgeneral(args[n-3], nextResults );
            return finalResults;
        }
    }

}
