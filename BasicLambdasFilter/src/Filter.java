import java.util.List;

public class Filter {

	public static void main(String[] args) {
		List<User> list = User.getUsers();
		System.out.println("Using findFirst() ---");
		User user = list.stream().filter(u -> u.getName().endsWith("na"))
				.findFirst().orElse(null);
		System.out.println(user.getName());
		
	}

}
