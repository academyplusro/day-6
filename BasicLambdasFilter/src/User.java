import java.util.ArrayList;
import java.util.List;

public class User{
	
private int id;
private String name;
private int age;

public User(int id, String name, int age) {
	this.id = id; 
	this.name = name;
	this.age = age;
}
public int getId() {
	return id;
}
public String getName() {
	return name;
}
public int getAge() {
	return age;
}

public static List<User> getUsers() {
	List<User> list = new ArrayList<User>();
	list.add(new User(1, "Diana", 20));
	list.add(new User(2, "Joanna", 15));
	list.add(new User(3, "Mary", 25));
	list.add(new User(4, "Elisabeth", 30));
	return list;
}
} 
